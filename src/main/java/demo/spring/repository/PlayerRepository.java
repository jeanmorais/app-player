package demo.spring.repository;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import demo.spring.domain.Player;

@Repository
public class PlayerRepository {

	private static final Logger log = LoggerFactory.getLogger(PlayerRepository.class);

	List<Player> players;

	@PostConstruct
	public void init() {
		players = Arrays.asList(
				new Player("Hugo Lloris", "GK", 1),
				new Player("Matts Hummels", "DF", 2),
				new Player("James Rodriguez", "MF", 2),
				new Player("Antoine Griezmann", "FW", 3),
				new Player("Kylian Mbappé", "FW", 4),
				new Player("Cristiano Ronaldo", "FW", 5));

	}

	public List<Player> findAll() {
		log.info("Total players found {}", this.players.size());
		return this.players;
	}

	public List<Player> findByTeam(Integer teamId) {
		log.info("Finding players. teamId={}", teamId);
		return players.stream().filter(player -> teamId.equals(player.getTeamId())).collect(Collectors.toList());
	}

}
