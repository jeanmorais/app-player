package demo.spring.domain;

public class Player {

	private String name;
	private String pos;
	private int teamId;

	public Player() {

	}

	public Player(String name, String pos, int teamId) {
		super();
		this.name = name;
		this.pos = pos;
		this.teamId = teamId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public int getTeamId() {
		return teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}
}
