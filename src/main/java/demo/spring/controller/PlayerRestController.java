package demo.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.spring.domain.Player;
import demo.spring.repository.PlayerRepository;

@RestController
@RequestMapping("/players")
public class PlayerRestController {
	
	@Autowired
	private PlayerRepository repository;
	
	@GetMapping
	private List<Player> getPlayers() {
		return repository.findAll();
	}
	
	@GetMapping("/team/{teamId}")
	private List<Player> getPlayersByTeam(@PathVariable int teamId) {
		return repository.findByTeam(teamId);
	}
}
